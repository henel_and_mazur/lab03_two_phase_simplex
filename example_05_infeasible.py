import logging
from saport.simplex.model import Model 

from saport.simplex.model import Model

def create_model() -> Model:
    model = Model("example_05_infeasible")
    #TODO:
    # fill missing test based on the example_03_unbounded.py
    # to make the test a bit more interesting:
    # * make sure model is infeasible
    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.add_constraint(2*x1 + 3*x2 + 1.5*x3 >= 30)
    model.add_constraint(x1 + 3*x2 >= 20)
    model.add_constraint(x1 <= 0)
    model.add_constraint(x2 <= 0)
    model.add_constraint(x3 <= 0)

    model.maximize(1000*x1 + 2400*x2 + 600*x3)
    return model

def run():
    model = create_model()
    #TODO:
    # add a test "assert something" based on the example_01_solvable.py
    # TIP: you may use other solvers (e.g. https://online-optimizer.appspot.com)
    #      to find the correct solution
    solution = model.solve()
    if solution.is_feasible:
        raise AssertionError("Your algorithm found a solution to an unbounded problem. This shouldn't happen...")
    else:
        logging.info("Congratulations! This problem is infeasible and your algorithm has found that :)")

    logging.info("This test is empty but it shouldn't be, fix it!")
    raise AssertionError("Test is empty")

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()
